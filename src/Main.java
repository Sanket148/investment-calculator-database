import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Scanner;

public class Main {

	static Connection con;
	static Statement stmt;
	static String custName;

	public static void main(String args[]) throws Exception {
		createDbConnection();
		Scanner sc = new Scanner(System.in);

		int option = 0;
int opt=0;
		while (opt != 3) {
			user();
			System.out.println("\n Enter Your Option : ");
			opt = sc.nextInt();

			if (opt == 1) {
				customer();
				
			} else if (opt == 2) {
				adminLog();
			} else if (opt == 3) {
				System.out.println("EXIT!!!!!");
				exit();
			} else {
				System.out.println("Re-Enter The Option!!!!!");
			}
			
			while (option != 3) {
				showOptionCust();
				System.out.println("\n Choose Your Option : ");
				option= sc.nextInt();

				if (option == 1) {
					sip();
				} else if (option == 2) {
					lumpsum();
				} else if (option == 3) {
					System.out.println("EXIT!!!!!");
					exit();
				} else {
					System.out.println("Re-Enter The Option!!!!!");
				}
			}
		}
	}

	public static void showOptionCust() {
		System.out.println("Choose Your Investment Plan");
		System.out.println("\t1.SIP" + "      *(It is monthly investment plan)");
		System.out.println("\t2.LUMPSUM" + "      *(It is one time investment plan)");
		System.out.println("\t3.EXIT");
	}

	public static void user() {
		System.out.println("***WELCOME***\n\n\n");
		System.out.println("\t1.CUSTOMER");
		System.out.println("\t2.ADMIN");
		System.out.println("\t3.EXIT");
	}

	public static void customer() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Your First Name :");
		String fName = sc.nextLine();
		System.out.println("Enter Your Last Name :");
		String lName = sc.nextLine();

		custName = (fName +"   "+ lName);
	}

	public static void adminLog() {

	}

	public static void sip() {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter Amount You Want To Invest PerMonth :  ");
		double investment = in.nextDouble();
		System.out.print("Input the rate of interest : ");
		double rate = in.nextDouble();
		System.out.print("Input Number Of Years : ");
		int year = in.nextInt();

		rate *= 0.01;
		double intrate=  rate;
		
            double monthsInvested=year*12;
			double amountAfterYear = investment * 12;
			double intrest = amountAfterYear * rate ;
			double finalAmt = amountAfterYear  + intrest;
			
			try {
				boolean rs1 = stmt.execute("insert into sip(custName, monthlyInvestment,monthsInvested,years,rate,intrest, roi) values ('" + custName + "', '"
						+ investment + "','"+monthsInvested+"','"+year+"','"+intrate+"','"+intrest+"','" + finalAmt + "');");

			} catch (Exception e) {
				System.out.println(e);
			}

			System.out.println("_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*\n\n");

		}


	public static void lumpsum() {
		Scanner in = new Scanner(System.in);
		System.out.print("Enter Lumpsum Amount You Want To Invest :  ");
		double investment = in.nextDouble();
		System.out.print("Input the rate of interest : ");
		double rate = in.nextDouble();
		System.out.print("Input Number Of Years : ");
		int year = in.nextInt();
		double rates= rate;
		rate *= 0.01;
		double intrest = investment * rate * year;
		double finalAmt = investment + intrest;

		

		try {
			boolean rs1 = stmt.execute("insert into lumpsum(custName, investment,years,rate,intrest, roi) values ('" + custName + "', '"
					+ investment + "','"+year+"','"+rates+"','"+intrest+"','" + finalAmt + "');");

		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println("_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*\n\n");
		
	}

	public static void createDbConnection() {

		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/investment_calculator", "root", "S@nket123");
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.println(e);

		}

	}

	public static void exit() throws Exception {

		System.exit(0);
	}
}
